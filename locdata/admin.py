# Copyright (C) 2011-2016 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import User
from django.db.models import TextField

from locdata.models import Country, Language, Locale, Value, Category


class UserAdmin(DjangoUserAdmin):
     list_display = DjangoUserAdmin.list_display + ('last_login',)


class CountryAdmin(admin.ModelAdmin):
    search_fields = ['name_upper']

class LanguageAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code_639_1', 'code_639_2b', 'code_639_2t', 'code_639_3']
    ordering = ('name',)
    list_display = ['name', 'code_639_1', 'code_639_2b', 'code_639_2t', 'code_639_3']

class ValueAdmin(admin.ModelAdmin):
    search_fields = ['value', 'comments', 'category__name']
    list_display = ('copy_or_value', 'category', 'locale', 'origin', 'comments')
    list_filter = ('locale',)

class ValueInline(admin.TabularInline):
    model = Value
    formfield_overrides = {
        TextField: {'widget': forms.Textarea(attrs={'rows':2})},
    }
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(ValueInline, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'origin':
            # FIXME: doesn't work currently
            field.widget.attrs['size'] = '10'
        return field

class LocaleAdmin(admin.ModelAdmin):
    raw_id_fields = ('language',)
    search_fields = ['code']
    filter_horizontal = ('editors',)
    inlines = [ValueInline]

class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['title', 'name', 'parent__name', 'posix_name']
    list_display = ['title', 'parent', 'name', 'posix_name', 'priority', 'weight']
    list_editable = ('weight',)
    ordering = ('weight',)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Language, LanguageAdmin)
admin.site.register(Locale, LocaleAdmin)
admin.site.register(Value, ValueAdmin)
admin.site.register(Category, CategoryAdmin)
