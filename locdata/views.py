# Copyright (C) 2011-2016 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import defaultdict

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Max
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext, TemplateDoesNotExist
from django.views.decorators.http import require_POST

from locdata import models

def locales(request):
    context = {
        'locales': models.Locale.objects.all().select_related('language', 'country'
                   ).annotate(cnt_origin=Count('value__origin', distinct=True)
                   ).annotate(first_origin=Max('value__origin')).order_by('code'),
    }
    return render(request, 'locales.html', context)

def locales_map(request):
    locales_by_country = defaultdict(list)
    qs = models.Locale.objects.exclude(country__isnull=True).exclude(country__code3=''
        ).select_related('language', 'country'
        ).annotate(cnt_origin=Count('value__origin', distinct=True)
        ).annotate(first_origin=Max('value__origin'))
    for loc in qs:
        locales_by_country[loc.country.code3].append(loc)
    context = {
        'locales': dict(locales_by_country),
        'access_token': settings.MAPBOX_TOKEN,
    }
    return render(request, 'map.html', context)

def locale(request, loc):
    loc = get_object_or_404(models.Locale, code=loc)
    main_categs = models.Category.objects.filter(parent__isnull=True).order_by('weight')
    values = {}
    for v in models.Value.objects.filter(locale=loc):
        values.setdefault(v.category.pk, []).append(v)
    # Populate subcategs and add values
    for cat in main_categs:
        # Handle values at cat level (only supports one value)
        cat_value = values.get(cat.pk, [])
        if cat_value and cat_value[0].copy_from:
            cat.copy_from = cat_value[0].copy_from
            try:
                copy_locale = models.Locale.objects.get(code=cat.copy_from)
                cat.copy_values = copy_locale.get_values(cat)
            except models.Locale.DoesNotExist:
                pass
            continue
        if cat_value and cat_value[0].value:
            cat.value = cat_value[0].value
            continue

        subcategs = list(models.Category.objects.filter(parent=cat).exclude(posix_name='').order_by('weight'))
        cat.subcategs = []
        cat.val_number = 0; cat.val_filled = 0
        for subcat in subcategs:
            subcat.current_values = values.get(subcat.pk, [])
            if not subcat.current_values and subcat.priority < 1:
                continue
            cat.subcategs.append(subcat)
            if subcat.current_values:
                cat.val_filled += 1
            cat.val_number += 1
        if cat.val_number == 0:
            cat.val_perc = "100%"
        else:
            cat.val_perc = "%.0f%%" % ((float(cat.val_filled) / float(cat.val_number)) * 100)

    context = {
        'locale': loc,
        'categs': main_categs,
        'editable': loc.can_edit(request.user),
    }
    return render(request, 'locale.html', context)

def get_widget(request, loc):
    loc = get_object_or_404(models.Locale, code=loc)
    categ = get_object_or_404(models.Category, pk=request.GET.get('cat'))
    if request.GET.get('val'):
        value = get_object_or_404(models.Value, pk=request.GET.get('val'))
        current_val = value.value
    else:
        current_val = None
    return HttpResponse(categ.get_widget(current_val))

@login_required
@require_POST
def save_value(request, loc):
    """ Save value and returns a table line as response """
    loc = get_object_or_404(models.Locale, code=loc)
    value = request.POST.get('value').replace('\r\n', '\n')
    categ_id = request.POST.get('cat')
    categ = get_object_or_404(models.Category, pk=categ_id)
    value_object, created = models.Value.objects.get_or_create(locale=loc, category=categ, origin='local')
    value_object.value = value
    value_object.save()
    context = {
        'subcat': value_object.category,
        'values':  models.Value.objects.filter(locale=loc, category=categ),
        'editable': True,
    }
    return render(request, 'value_line.html', context)


@login_required
@require_POST
def delete_value(request, loc):
    value_pk = request.POST.get('value')
    value = get_object_or_404(models.Value, pk=value_pk)
    value.delete()
    return HttpResponse()


def locale_glibc(request, loc):
    loc = get_object_or_404(models.Locale, code=loc)
    response = HttpResponse(loc.format_glibc(), content_type="text/plain; charset=utf-8")
    return response

def iso639(request):
    context = {'isolist': models.Language.all()}
    return render(request, 'iso639.html', context)

def help(request, name):
    try:
        return render(request, 'help/%s.html' % name)
    except TemplateDoesNotExist:
        raise Http404

def cat_values(request, name):
    cat = get_object_or_404(models.Category, name=name)
    context = {
        'subcat': cat,
        'values': cat.value_set.all().order_by('locale__code')
    }
    return render(request, 'cat_values.html', context)
