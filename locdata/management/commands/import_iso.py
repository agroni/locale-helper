import os

from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = "Script to import data from ISO data files"

    def add_arguments(self, parser):
        parser.add_argument('input_file')

    def handle(self, *args, **options):
        filename = options['input_file']
        if not os.access(filename, os.R_OK):
            raise CommandError("Unable to find a file named '%s'" % filename)
        if 'iso3166' in filename:
            return self.import3166(filename)
        elif '639-2' in filename:
            return self.import639_2(filename)
        elif '639-3' in filename:
            return self.import639_3(filename)
        else:
            raise CommandError("Unable to determine which sort of iso data the file '%s' contains" % filename)

    def import3166(self, filename):
        from locdata.models import Country
        count = 0
        with open(filename, encoding='latin-1') as fp:
            # Skip first two lines
            next(fp); next(fp)
            for line in fp:
                try:
                    cname, ccode = line.split(';',1)
                except ValueError:
                    continue
                c = Country(code=ccode.strip(), name_upper=cname)
                c.save()
                count += 1
        return "%d lines imported in Country model" % count
            
    def import639_2(self, filename):
        from locdata.models import Language
        count = 0
        with open(filename) as fp:
            # Skip first line
            next(fp)
            for line in fp:
                iso2, iso1, english, french = line.split('\t')
                
                if "/" in iso2:
                    iso2b, iso2t = iso2.split("/")
                    iso2b, iso2t = iso2b[:-4], iso2t[:-4]
                else:
                    iso2b, iso2t = iso2, iso2

                # Try to find an existing ISO 639-1 entry
                if iso1 != "":
                    # Should be there already, verify it
                    try:
                        lang = Language.objects.get(code_639_1=iso1)
                    except Language.DoesNotExist:
                        self.stderr.write("Unable to find existing language for code '%s'" % iso1)
                        continue
                
                # Try to find existing ISO 639-2 entry
                try:
                    lang = Language.objects.get(code_639_2b=iso2b, code_639_2t=iso2t)
                except Language.DoesNotExist:
                    #print "A new language (name='%s', iso2b='%s', iso2t='%s') will be created" % (
                    #    english, iso2b, iso2t)
                    lang = Language(name=english, code_639_2b=iso2b, code_639_2t=iso2t, code_639_1=iso1)
                    lang.save()
                    count += 1
                else:
                    #print "Existing entry found for %s/%s (%s)" % (iso2b, iso2t, english)
                    continue
        return "%d lines imported in Language model" % count


    def import639_3(self, filename):
        from locdata.models import Language
        count = 0
        with open(filename) as fp:
            # Skip first line
            next(fp)
            for line in fp:
                iso3, iso2b, iso2t, iso1, scope, ltype, name, comment = line.split('\t')
                comment = comment.strip()
                Language.objects.get_or_create(name=name, defaults={
                    'code_639_3': iso3, 'code_639_2b': iso2b, 'code_639_2t': iso2t,
                    'code_639_1': iso1, 'scope': scope, 'lang_type': ltype,
                    'comment': comment})
                count += 1
        return "%d lines imported in Language model" % count
