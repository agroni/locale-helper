import os

from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse

from locdata.models import (
    Category, Country, Language, Locale, Value, format_comments,
)


class LocaleTest(TestCase):
    fixtures = ["categories.json"]

    def setUp(self):
        Country.objects.bulk_create([
            Country(code='AR', name_upper="ARGENTINA"),
            Country(code='DZ', name_upper="ALGERIA"),
            Country(code='TW', name_upper="TAIWAN"),
        ])
        Language.objects.bulk_create([
            Language(name="Arabic", code_639_1="ar"),
            Language(name="Spanish", code_639_1="es"),
            Language(name="Literary Chinese", code_639_3="lzh"),
        ])
        self.admin_user = User.objects.create_superuser('admin', None, 'nimda')

    def test_locale_list(self):
        locales = [
            Locale.objects.create(
                language=Language.objects.get(code_639_1='ar'),
                country=Country.objects.get(code='DZ')
            ),
            Locale.objects.create(
                language=Language.objects.get(code_639_1='es'),
                country=Country.objects.get(code='AR')
            ),
            Locale.objects.create(
                language=Language.objects.get(code_639_3='lzh'),
                country=Country.objects.get(code='TW')
            ),
        ]
        values = []
        for locale in locales:
            values.extend([
                Value(locale=locale, category=Category.objects.get(name="name_fmt"),
                      value="%f%m%d", origin="glibc-HEAD", in_glibc=True),
                Value(locale=locale, category=Category.objects.get(name="name_gen"),
                      value="Mr.", origin="glibc-HEAD", in_glibc=True),
                Value(locale=locale, category=Category.objects.get(name="height"),
                      value="297", origin="glibc-HEAD", in_glibc=True),
            ])
        values[0].origin = 'local' # -> mixed
        Value.objects.bulk_create(values)

        with self.assertNumQueries(1):
            response = self.client.get(reverse('locales'))
        self.assertContains(response,
            '<tr class="mixed"><td><a href="/locale/ar_DZ/">ar_DZ</a></td>'
            '<td>— Arabic —</td><td>ALGERIA</td>'
            '<td class="origin">mixed</td></tr>', html=True)
        self.assertContains(response,
            '<tr class="glibc-HEAD"><td><a href="/locale/lzh_TW/">lzh_TW</a></td>'
            '<td>— Literary Chinese —</td><td>TAIWAN</td>'
            '<td class="origin">glibc-HEAD</td></tr>', html=True)

    def test_import_glibc_local(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "lzh_TW")
        call_command('import_glibc', path, origin='local')
        loc = Locale.objects.get(language__code_639_3='lzh', country__code='TW')
        self.assertEqual(loc.get_value('am_pm', 'local').value, "朝\n暮")
        self.assertEqual(loc.get_value('t_fmt', 'local').value, "%OH時%OM分%OS秒")
        # Test read double slash
        self.assertEqual(loc.get_value('postal_fmt', 'local').value,
            "%c/%N%T%N%s %h %e %r%N%b%N%d%N%f%N%a%N")

    def test_no_country_locale(self):
        eo_lang = Language.objects.create(name="Esperanto", code_639_1="eo")
        eo_locale = Locale.objects.create(language=eo_lang)
        self.assertEqual(str(eo_locale), 'eo')

    def test_format_comments(self):
        self.assertEqual(format_comments(''), '')
        self.assertEqual(format_comments(None), '')
        self.assertEqual(format_comments("Some comment"), "% Some comment\n")
        self.assertEqual(format_comments("% Some comment"), "% Some comment\n")
        self.assertEqual(
            format_comments("Multi\nLine\n\nComment"),
            "% Multi\n% Line\n% \n% Comment\n"
        )
        self.assertEqual(
            format_comments("% Multi\r\nLine\r\n\r\n% Comment"),
            "% Multi\n% Line\n% \n% Comment\n"
        )

    def test_value_for_glibc(self):
        loc = Locale.objects.create(
            language=Language.objects.get(code_639_3='lzh'),
            country=Country.objects.get(code='TW')
        )
        for value, expected in [
                ('中華民國', ('"<U4E2D><U83EF><U6C11><U570B>"', '% 中華民國\n')),
                ('TWN', ('"TWN"', '')),
                ('%d/%m/%Y', ('"%d//%m//%Y"', '')),
                ('<a>', ('"<U003C>a<U003E>"', '% <a>\n'))]:
            self.assertEqual(
                Value(locale=loc, category=Category.objects.first(), value=value).value_for_glibc(),
                expected
            )
        # int_curr_symbol has fixed 4 chars
        val = Value(
            locale=loc,
            category=Category.objects.get(name="int_curr_symbol", posix_name="int_curr_symbol"),
            value='BGN'
        )
        # week doesn't need quotes
        val = Value(
            locale=loc,
            category=Category.objects.get(name="week", posix_name="week"),
            value='7;19971130;4'
        )
        self.assertEqual(val.value_for_glibc(), ('7;19971130;4', ''))

    def test_delete_value(self):
        loc = Locale.objects.create(
            language=Language.objects.get(code_639_3='lzh'),
            country=Country.objects.get(code='TW')
        )
        val = Value.objects.create(locale=loc, category=Category.objects.first(), value="Blah")
        self.assertTrue(self.client.login(username='admin', password='nimda'))
        response = self.client.post(
            reverse('delete-value', args=["lzh_TW"]), {'value': val.pk}
        )
        self.assertEqual(response.content, b'')
        with self.assertRaises(Value.DoesNotExist):
            Value.objects.get(pk=val.pk)
