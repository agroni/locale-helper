# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('posix_name', models.CharField(max_length=50, blank=True)),
                ('title', models.CharField(max_length=200)),
                ('en_value', models.CharField(max_length=200, blank=True)),
                ('multiline', models.IntegerField(default=0)),
                ('priority', models.IntegerField(default=0)),
                ('weight', models.IntegerField(default=0)),
                ('cldr_path', models.CharField(max_length=200, blank=True)),
                ('choices', models.TextField(blank=True)),
                ('comments', models.TextField(blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', models.ForeignKey(related_name='children', blank=True, to='locdata.Category', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=3)),
                ('name_upper', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('code_639_3', models.CharField(max_length=3, blank=True)),
                ('code_639_2b', models.CharField(max_length=3, blank=True)),
                ('code_639_2t', models.CharField(max_length=3, blank=True)),
                ('code_639_1', models.CharField(max_length=2, blank=True)),
                ('scope', models.CharField(blank=True, max_length=1, choices=[('I', 'Individual'), ('M', 'Macrolanguage'), ('S', 'Special'), ('C', 'Collective')])),
                ('lang_type', models.CharField(blank=True, max_length=1, choices=[('A', 'Ancient'), ('C', 'Constructed'), ('E', 'Extinct'), ('H', 'Historical'), ('L', 'Living'), ('S', 'Special')])),
                ('comment', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Locale',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=20, blank=True)),
                ('script', models.CharField(max_length=20, blank=True)),
                ('country', models.ForeignKey(to='locdata.Country', on_delete=models.CASCADE)),
                ('editors', models.ManyToManyField(to=settings.AUTH_USER_MODEL, blank=True)),
                ('language', models.ForeignKey(to='locdata.Language', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Value',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(blank=True)),
                ('copy_from', models.CharField(default='', max_length=10, blank=True)),
                ('origin', models.CharField(default='local', max_length=50)),
                ('in_cldr', models.BooleanField(default=False)),
                ('in_glibc', models.BooleanField(default=False)),
                ('comments', models.TextField(null=True, blank=True)),
                ('category', models.ForeignKey(to='locdata.Category', on_delete=models.CASCADE)),
                ('locale', models.ForeignKey(to='locdata.Locale', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='value',
            unique_together=set([('locale', 'category', 'origin')]),
        ),
        migrations.AddField(
            model_name='comment',
            name='value',
            field=models.ForeignKey(to='locdata.Value', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
