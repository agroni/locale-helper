from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('locdata', '0003_country_fk_nullable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locale',
            name='language',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='locdata.Language'),
        ),
    ]
