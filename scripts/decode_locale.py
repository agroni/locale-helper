#!usr/bin/env python
import re
import sys
import os

def uni_from_hex(match):
    return unichr(int(match.group(1), 16))

if len(sys.argv) != 2:
    print "Usage: decode_locale.py path_to_locale"
    sys.exit(1)

loc = sys.argv[1]
if not os.path.exists(loc):
    print "Unable to find locale file '%s'" % loc
    sys.exit(1)

fp = open(loc)
for line in fp:
    print re.sub(r'<U([\dA-F]{4})>', uni_from_hex, line.strip())
    #print line
fp.close()
