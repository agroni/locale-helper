#!usr/bin/env python

# install_locale.py, a script to install locales on Linux systems
# Copyright (C) 2011, Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import fileinput

if len(sys.argv) != 2:
    print "Usage: install_locale.py [path_to_locale|URL]"
    sys.exit(1)
if not os.access("/usr/lib/locale/", os.W_OK):
    print "Error: You should run this command as root (or with sudo)"
    sys.exit(1)

locale_file = sys.argv[1]
locale = locale_file.rsplit('/', 1)[-1].split('.', 1)[0]
supported_file = '/usr/share/i18n/SUPPORTED'

def add_locale_to_supported(locale):
    """ Add locale to supported locale list """
    is_already = False
    for line in fileinput.input(supported_file, inplace=1):
        if not is_already:
            if line.startswith(locale):
                is_already = True
            elif line > locale:
                print "%s.UTF-8 UTF-8" % locale
                is_already = True
        print line,

if locale_file.startswith("http"):
    import urllib2
    u = urllib2.urlopen(locale_file)
    with open(locale, 'w') as loc_file:
        loc_file.write(u.read())
    locale_file = os.path.abspath(loc_file.name)

cmd = "localedef -i %(locale_file)s --charmap=UTF-8 %(locale)s.UTF-8" % locals()
res = os.system(cmd)
if res == 0:
    print "%s locale successfully installed on your system" % locale

# Try to add locale to SUPPORTED
if not os.path.exists(supported_file):
    print "We don't know where is the SUPPORTED file on your system. Skipping..."
else:
    add_locale_to_supported(locale)

