from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

from locdata import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='index.html'), name="home"),
    path('accounts/', include('django.contrib.auth.urls')),

    path('locales/', views.locales, name='locales'),
    path('locales/map/', views.locales_map, name='locales-map'),
    path('locale/<loc>/', views.locale, name='locale'),
    path('locale/<loc>/get_widget/', views.get_widget),
    path('locale/<loc>/save_value/', views.save_value, name="save-value"),
    path('locale/<loc>/delete_value/', views.delete_value, name="delete-value"),
    path('locale/<loc>/glibc/', views.locale_glibc, name='locale_glibc'),
    path('iso639/', views.iso639),
    path('help/<name>/', views.help, name='help'),

    path('values/<name>/', views.cat_values, name='cat_values'),
]
